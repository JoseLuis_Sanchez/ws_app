from django.shortcuts import render
import requests
import json
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.models import User
from reconocimiento.models import profileuser, tickets
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from datetime import date, datetime
from rest_framework import status
from rest_framework.status import (HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_200_OK, HTTP_201_CREATED, HTTP_202_ACCEPTED)
import time
from django.conf import settings
import base64
from base64 import b64decode
from django.core.files.base import ContentFile
import os

@api_view(['POST'])
def createuser(request):
    usr = json.loads(request.body)
    if profileuser.objects.filter(email = usr['email']).exists():
        return Response({'status': 'duplicate'})
    else:
        us = profileuser()
        us.username = usr['username']
        us.last_name = usr['last_name']
        us.email = usr['email']
        us.telephone = usr['phone']
        try:
            os.remove(os.path.join(settings.MEDIA_ROOT+'/profile/'+usr['name_image']))
        except FileNotFoundError:
            print('error | FileNotFoundError')

        data = ContentFile(base64.b64decode(usr['image']), name = usr['name_image'])
        us.photo = data
        us.save()

        return Response({'status': 'success'})

@api_view(['GET'])
def profiles(request):
    profile = profileuser.objects.all()
    phot = []
    for i in profile:
        phot.append(str(i.photo))

    return Response({'status': phot})

@api_view(['GET'])
def profilesall(request):
    profile = profileuser.objects.all()
    phot = []
    for i in profile:
        # print('->', os.path.join(settings.BASE_DIR)+str(i.photo))
        phot.append({'image': str(i.photo), 'id': i.idprofile, 'username': i.username})
    return Response({'data': phot})

@api_view(['GET'])
def addticket(request, *args, **kwargs):
    pk = request.query_params.get('id')
    temp = request.query_params.get('temp')

    if profileuser.objects.filter(idprofile = pk):
        pro = tickets()
        pro.user_id = pk
        pro.ticket = temp
        pro.date = datetime.now()
        pro.save()

        return Response({'status': 'success'})  
    else:
        return Response({'status': 'error'})  

"""    
@api_view(['POST'])
def updatePhoto(request):
    req = json.loads(request.body)

    if User.objects.filter(id = req['userid']).exists():
        try:
            if profileuser.objects.filter(user_id = req['userid']).exists():
                pro = profileuser.objects.get(user_id = req['userid'])
                try:
                    os.remove(os.path.join(settings.MEDIA_ROOT+'/profile/'+req['name']))
                except FileNotFoundError:
                    print('error')
                data = ContentFile(base64.b64decode(req['photo']), name = req['name'])
                pro.photo = data
                pro.save()
                # imag = str(pro.photo)
                return Response({'status': 'update_success'})
            else:
                pro = profileuser()
                data = ContentFile(base64.b64decode(req['photo']), name = req['name'])
                pro.photo = data
                pro.save()
                imag = str(pro.photo)
                return Response({'status': 'create_success'})
                
        except IndexError as e:
            print('=>', e)
            return Response({'status': 'error'})
    else:
        return Response({'status': 'not found'}, status=status.HTTP_404_NOT_FOUND)
"""