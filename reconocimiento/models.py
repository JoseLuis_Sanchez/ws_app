from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class profileuser(models.Model):
    idprofile = models.AutoField(primary_key=True)
    username = models.TextField()
    last_name = models.TextField()
    email = models.TextField()
    telephone = models.TextField()
    photo = models.ImageField(upload_to="profile/")

class tickets(models.Model):
    idticket = models.AutoField(primary_key=True)
    user = models.ForeignKey(profileuser, on_delete=models.CASCADE)
    ticket = models.TextField()
    date = models.TextField()
