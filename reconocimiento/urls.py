from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^createuser', views.createuser),
    # url(r'^updatePhoto', views.updatePhoto),
    url(r'^profiles', views.profiles),
    url(r'^loadprofiles', views.profilesall),
    url(r'^addticket', views.addticket)
    # url(r'^load', views.load_script),
]